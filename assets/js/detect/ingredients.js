import {
    first,
    trim,
    sprintf,
} from 'voca';

const elementSelectors = [
    'div:contains("%s")',
    'li:contains("%s")',
];

/**
 * Find ingredient list in DOM
 *
 * @param {String} keyword Uses keyword to detect ingredient lists
 *
 * @returns {String|Boolean}
 */
export const detectIngredients = (keyword) => {
    let outcome = false;

    elementSelectors.some((selector) => {
        let elements = $(sprintf(selector, keyword));

        if (elements.length === 0) {
            return false;
        }

        let txt = elements.last().text();
        txt = txt.slice(txt.indexOf(keyword)).slice(keyword.length);
        txt = trim(first(txt, txt.indexOf('.')), ': \n');
        if (txt) {
            outcome = txt;
            return true;
        }
        return false;
    });
    
    return outcome;
};