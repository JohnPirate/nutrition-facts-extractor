import {forEachRight} from 'lodash';
import {includes} from 'voca';


/**
 * Takes a HTMLElement and convert it to text only
 *
 * @param {HTMLElement} domElement
 *
 * @returns {string}
 */
export const normalizeDomToTxt = (domElement) => {
    let txt = '';
    $(domElement)
        .text()
        .split('\n')
        .forEach((line) => {
            line = line.trim();
            if (line) {
                txt += line + '\n';
            }
        })
    ;
    return txt;
};

/**
 * Find nutrition facts in DOM
 *
 * @param {String} keyword Used keyword to detect a nutrition facts table
 * @param {null|JQuery<HTMLElement>} element A JQuery element to search from his parent downwards for nutrition facts
 * @param {Number} depth Max depth of recursion
 *
 * @returns {String[]|String|boolean}
 */
export const detectNutritionFacts = (keyword, element = null, depth = 3) => {

    if (depth === 0) {
        console.log('detectNutritionFacts - max depth reached.');
        return false;
    }

    let elements;

    if (element) {
        elements = element.parent('div');
    } else {
        elements = $(`div:contains("${keyword}")`);
    }

    if (elements.length === 0) {
        return false;
    }

    let txts = [];
    forEachRight(elements, (elem) => {
        let txt = normalizeDomToTxt(elem);
        if (txts.length > 0) {
            txts.forEach((t) => {
                if (!includes(txt, t)) {
                    txts.push(txt);
                }
            });
        } else {
            txts.push(txt);
        }
    });

    // Check if parent element has better result
    if (txts.length <= 1) {

        let found = detectNutritionFacts(keyword, elements, depth - 1);

        if (Array.isArray(found) && found.length === 1) {
            if (found[0].length > txts[0].length) {
                txts = found;
            }
        } else if (found && found.length > txts[0].length) {
            txts[0] = found;
        }

        return txts[0];
    }

    return txts;
};