import {get, eachRight, replace} from 'lodash';
import {translations} from './translations';

/**
 * @param {string} string
 * @param {Object} args
 * @return {string}
 */
export function trans(string, args) {
    let value = get(translations, string, string);
    eachRight(args, (paramVal, paramKey) => {
        value = replace(value, `:${paramKey}`, paramVal);
    });
    return value;
}