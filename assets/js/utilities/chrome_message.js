export const sendMessageRefActiveTab = (action, data, cb) => {
    chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
        if (tabs.length > 0) {
            const key = `${tabs[0].windowId}_${tabs[0].id}`;
            chrome.runtime.sendMessage({key, action, data}, cb);
        }
    });
};

export const sendMessageToActiveTab = (action, data, cb) => {
    chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
        if (tabs.length > 0) {
            chrome.tabs.sendMessage(tabs[0].id, {action, data}, cb);
        }
    });
};