import {has} from './helper';

/**
 * @class KeyValStore
 */
export class KeyValStore {
    constructor() {
        this.store = {};
    }

    has(key) {
        return has.call(this.store, key);
    }

    set(key, val) {
        this.store[key] = val;
    }

    get(key) {
        return this.store[key];
    }

    all() {
        return this.store;
    }

    remove(key) {
        delete this.store[key];
    }
}