import {isArray} from 'lodash';

export const Str = {
    contains(haystack, needles) {
        needles = isArray(needles) ? needles : [needles];
        return needles.some((needle) => {
            return haystack.indexOf(needle) !== -1;
        });
    },
};