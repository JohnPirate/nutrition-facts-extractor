export const getCurrentTab = () => {
    let currentTab = null;
    chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
        if (tabs.length > 0) {
            currentTab = tabs[0];
        }
    });
    return currentTab;
};