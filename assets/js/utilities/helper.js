import {replace} from 'lodash';
import {Str} from './str';

export const has = Object.prototype.hasOwnProperty;

export const VALID_NUTRIENT_UNITS = [
    'kcal',
    'ng', 'µg', 'mg', 'g', 'dag',
];

/**
 * @param {String} str
 * @returns {Number}
 */
export const strToNumeric = (str) => {
    return parseFloat(replace(str, ',', '.'));
};

/**
 * @param {Object} obj
 * @param {String} key
 * @param {Array}  skip
 * @returns {null|Object|Number|String|Array}
 */
export const getValueFromObject = (obj, key, skip = []) => {
    let value = null;
    Object.keys(obj).some((_key) => {
        if (Str.contains(key, _key)) {
            if (skip.indexOf(obj[_key]) === -1) {
                value = obj[_key];
                return true;
            }
        }
        return false;
    });
    return value;
};

/**
 * @param {String} str
 * @returns {{unit: String, value: Number}|null}
 */
export const detectNutrientDetails = (str) => {
    const units = VALID_NUTRIENT_UNITS.join('|');
    const regex = new RegExp(`([\\d\,\.]+)[\\s\|]*(${units})`, 'gi');
    const m = regex.exec(str);
    if (m === null) {
        return null;
    }
    return {
        value: strToNumeric(m[1]),
        unit: m[2]
    };
};

/**
 * @param {String} str
 * @returns {Number[]}
 */
export const detectNumericValues = (str) => {
    const regex = new RegExp(`(([a-zA-Z]*)[\( \|]?([\\d\.\,]+)[\) \|]?)`, 'gi');
    let m;
    const values = [];
    while ((m = regex.exec(str)) !== null) {
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        if (typeof m[2] === 'undefined' || !m[2]) {
            values.push(strToNumeric(m[3]));
        }
    }
    return values;
};

/**
 * @param {String} str
 * @returns {String|null}
 */
export const detectNutrientUnits = (str) => {
    const units = VALID_NUTRIENT_UNITS.join('|');
    const regex = new RegExp(`\\W+\\(?(${units})\\.*`, 'gi');
    const m = regex.exec(str);
    if (m === null) {
        return null;
    }
    return m[1];
};