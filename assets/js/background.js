import {
    ACTION_FOUND_INGREDIENTS,
    ACTION_FOUND_NUTRIENTS,
    ACTION_GET_FOOD_DATA,
    ACTION_NEW_FOOD_DATA,
    ACTION_UPDATE_FOOD_DATA,
} from './actions';
import {has} from './utilities/helper';
import {KeyValStore} from './utilities/store';
import {getCurrentTab} from './utilities/chrome_helper';

const store = new KeyValStore();

const createFood = () => {
    return {
        name: '',
        brand: '',
        ref_value: 100,
        ref_unit: 'g',
        carb_without_fiber: 1,
        nutrientsParser: 'default',
        nutrientsRaw: '',
        nutrients: {},
        source: '',
        ingredientsParser: 'default',
        ingredientsRaw: '',
        ingredients: [],
        portionsRaw: '',
        portions: [],
    };
};

/*
|---------------------------------------------------------------------------
| Receive Food Infos
|---------------------------------------------------------------------------
*/
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    console.log('=== request: ', request);
    console.log('=== sender: ', sender);

    if (request.action === ACTION_GET_FOOD_DATA) {
        if (has.call(request, 'key') && store.has(request.key)) {
            sendResponse(store.get(request.key));
        } else {
            sendResponse({});
        }
        return;
    }

    if (request.action === ACTION_UPDATE_FOOD_DATA) {
        if (has.call(request, 'key') && store.has(request.key)) {
            const data = request.data;
            const ref = store.get(request.key);
            console.log('=== ref: ', ref);
            Object.keys(data).forEach((key) => {
                if (has.call(ref.food, key)) {
                    ref.food[key] = data[key];
                }
            });
            store.set(request.key, ref);
            sendResponse(true);
        } else {
            sendResponse(false);
        }
        return;
    }

    if (request.action === ACTION_NEW_FOOD_DATA) {

        if (!has.call(request.data, 'key')) {
            sendResponse(false);
            return;
        }

        const key = request.data.key;

        let food = createFood();

        food.source = request.data.source;

        let data = {
            nutritionFacts: '1',
            food,
        };
        store.set(key, data);
        sendResponse(true);
        return;
    }

    // Need a tab to be continued
    if (!has.call(sender, 'tab')) {
        return;
    }

    const key = `${sender.tab.windowId}_${sender.tab.id}`;

    if (request.action === ACTION_FOUND_NUTRIENTS) {

        let food = createFood();

        food.nutrients = request.nutrients;
        food.nutrientsRaw = request.nutrientsRaw;
        food.source = sender.tab.url;

        let data = {
            nutritionFacts: '1',
            food,
        };
        store.set(key, data);
    }
    else if (request.action === ACTION_FOUND_INGREDIENTS) {
        let data = store.get(key);
        data.food.ingredients = request.ingredients;
        data.food.ingredientsRaw = request.ingredientsRaw;
        store.set(key, data);
    }
});

/*
|---------------------------------------------------------------------------
| Additional Badge Handling
|---------------------------------------------------------------------------
*/
chrome.tabs.onActivated.addListener(({tabId, windowId}) => {

    const key = `${windowId}_${tabId}`;

    if (store.has(key)) {
        let {nutritionFacts} = store.get(key);
        chrome.browserAction.setBadgeText({text: nutritionFacts});
    } else {
        chrome.browserAction.setBadgeText({text: ''});
    }
});

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {

    const key = `${tab.windowId}_${tab.id}`;

    if (changeInfo.status === 'loading') {
        store.remove(key);
    }

    if (store.has(key)) {
        let {nutritionFacts} = store.get(key);
        chrome.browserAction.setBadgeText({text: nutritionFacts});
    } else {
        chrome.browserAction.setBadgeText({text: ''});
    }
});

chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
    const key = `${removeInfo.windowId}_${tabId}`;
    store.remove(key);
});