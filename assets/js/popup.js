import Vue from 'vue';
window.Vue = Vue;

import {trans} from './utilities/localization';
window.$trans = trans;
Vue.prototype.$trans = trans;

import PopupView from './vue/popup_view';

import {
    ACTION_GET_FOOD_DATA,
    VUE_ACTION_SET_FOOD_DATA,
} from './actions';
import {has} from './utilities/helper';
import {sendMessageRefActiveTab} from './utilities/chrome_message';

const app = new Vue({render: h => h(PopupView)});
window.app = app;

document.addEventListener('DOMContentLoaded', () => {
    app.$mount('#root');
    sendMessageRefActiveTab(ACTION_GET_FOOD_DATA, null, (response) => {
        if (response && has.call(response, 'food')) {
            app.$emit(VUE_ACTION_SET_FOOD_DATA, response.food);
        }
    });
});