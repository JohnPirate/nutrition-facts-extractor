import {sendMessageRefActiveTab, sendMessageToActiveTab} from '../../utilities/chrome_message';
import {
    ACTION_GET_FOOD_DATA,
    ACTION_NEW_FOOD_DATA,
    ACTION_RESCAN_DOM,
    ACTION_UPDATE_FOOD_DATA,
    VUE_ACTION_SET_FOOD_DATA,
} from '../../actions';
import {has} from '../../utilities/helper';
import {nutritionExtractData} from '../../extract/nutrition';
import {mustHaveNutrients} from '../../config/nutrients';
import {ingredientsExtractData} from '../../extract/ingredients';
import {extractPortionsData} from '../../extract/portions';

export default {

    created() {
        const vm = this;
        const hub = this.$root;

        hub.$on(VUE_ACTION_SET_FOOD_DATA, (data) => {
            vm.$set(vm, 'foodRef', data);
        });
    },

    data() {
        return {
            foodRef: {},
            searchForNutritionFacts: false,
        };
    },

    computed: {
        hasFood() {
            return Object.keys(this.foodRef).length > 0;
        },
        food() {
            return this.foodRef;
        },
        hasNutrients() {
            return Object.keys(this.getParsedNutrients).length > 0;
        },
        nutrients() {
            return this.getParsedNutrients;
        },
        hasIngredients() {
            return this.ingredients.length > 0;
        },
        ingredients() {
            return this.getParsedIngredients;
        },
        hasPortions() {
            return this.portions.length > 0;
        },
        portions() {
            return this.getParsedPortions;
        },
        doesSearchForNutritionCalls() {
            return this.searchForNutritionFacts;
        },
        getParsedNutrients() {
            if (this.food.nutrientsParser === 'hofer_backbox') {
                let txt = this.food.nutrientsRaw.replace(/[\/(]/ig, '\n');
                let nutrients = nutritionExtractData('Energie '+txt);
                if (!has.call(nutrients, 'fiber_total_dietary')) {
                    nutrients.fiber_total_dietary = {value:0,unit:'g'};
                }
                return nutrients;
            }
            return nutritionExtractData(this.food.nutrientsRaw);
        },
        getParsedIngredients() {
            return ingredientsExtractData(this.food.ingredientsRaw);
        },
        getParsedPortions() {
            return extractPortionsData(this.food.portionsRaw);
        },
    },

    methods: {
        isMustHaveNutrient(nutrienKey) {
            return mustHaveNutrients.indexOf(nutrienKey) !== -1;
        },
        getNutrient(nutrientKey) {
            if (this.hasNutrient(nutrientKey)) {
                return this.nutrients[nutrientKey];
            }
            return {
                unit: '-',
                value: '',
            };
        },
        hasNutrient(nutrientKey) {
            return has.call(this.nutrients, nutrientKey);
        },
        updateFoodData(key, val) {
            const vm = this;
            const data = {};
            data[key] = val;
            sendMessageRefActiveTab(ACTION_UPDATE_FOOD_DATA, data, (response) => {
                console.log(response);
                if (response) {
                    vm.foodRef[key] = val;
                }
            });
        },
        loadData() {
            console.log('load data');
            const hub = this.$root;
            sendMessageRefActiveTab(ACTION_GET_FOOD_DATA, null, (response) => {
                if (response && has.call(response, 'food')) {
                    console.log(response);
                    hub.$emit(VUE_ACTION_SET_FOOD_DATA, response.food);
                }
            });
        },
        scanDom() {
            const vm = this;
            vm.searchForNutritionFacts = true;
            sendMessageToActiveTab(ACTION_RESCAN_DOM, null, (response) => {
                vm.loadData();
                setTimeout(() => {
                    vm.searchForNutritionFacts = false;
                }, 300);
            });
        },
        newFood() {
            const vm = this;
            chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {
                if (tabs.length > 0) {
                    const currentTab = tabs[0];
                    const data = {
                        key: `${currentTab.windowId}_${currentTab.id}`,
                        source: currentTab.url,
                    };
                    sendMessageRefActiveTab(ACTION_NEW_FOOD_DATA, data, (response) => {
                        console.log(response);
                        if (response) {
                            vm.loadData();
                        }
                    });
                }
            });

        }
    }
}