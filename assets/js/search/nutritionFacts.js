import {detectNutritionFacts} from '../detect/nutritionFacts';
import {nutritionExtractData} from '../extract/nutrition';
import {ACTION_FOUND_NUTRIENTS} from '../actions';
import {ingredientsSearchKeywords} from '../config/search';
import {searchIngredientLists} from './ingredientLists';

export const searchNutritionFacts = (keyword) => {
    console.log('##### Nutrition keyword: ', keyword, ' #####');

    console.log('- Try to find nutrition facts');
    let facts = detectNutritionFacts(keyword);

    if (!facts) {
        console.log('- No nutrition facts found');
        return false;
    }

    console.log('- Nutrition facts found:');
    console.log(facts);

    if (!Array.isArray(facts)) {
        facts = [facts];
    }

    let nutrients = null;
    let nutrientsRaw = null;

    console.log('- Try to extract nutrients');
    facts.some((fact) => {
        nutrients = nutritionExtractData(fact);
        if (Object.keys(nutrients).length > 0) {
            nutrientsRaw = fact;
            return true;
        }
    });

    if (Object.keys(nutrients).length > 0) {
        console.log('- Nutrients found:');
        console.log(nutrients);

        chrome.runtime.sendMessage({
            action: ACTION_FOUND_NUTRIENTS,
            nutrients,
            nutrientsRaw,
        });

        ingredientsSearchKeywords.some(searchIngredientLists);

        return true;
    }
};