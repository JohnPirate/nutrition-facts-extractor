import {detectIngredients} from '../detect/ingredients';
import {ingredientsExtractData} from '../extract/ingredients';
import {ACTION_FOUND_INGREDIENTS} from '../actions';

export const searchIngredientLists = (keyword) => {

    console.log('##### Ingredient keyword: ', keyword, ' #####');

    console.log('- Try to find ingredients list');
    let ingredientsRaw = detectIngredients(keyword);

    if (!ingredientsRaw) {
        console.log('- No ingredients list found');
        return false;
    }

    console.log('- Raw ingredients list found:');
    console.log(ingredientsRaw);

    console.log('- Try to parse raw ingredients list');
    let ingredients = ingredientsExtractData(ingredientsRaw);

    if (ingredients.length > 0) {
        console.log('- Ingredients found:');
        console.log(ingredients);
        chrome.runtime.sendMessage({
            action: ACTION_FOUND_INGREDIENTS,
            ingredients,
            ingredientsRaw,
        });
        return true;
    }
};