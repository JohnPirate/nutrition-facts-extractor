export const nutritionSearchKeywords = [
    'Energie',
    'Energy',
    'Brennwert',
    'Nährwertangaben',
    'kcal',
    'Fett',
    'Fat',
    'Nährstoffe',
];

export const ingredientsSearchKeywords = [
    'Zutaten',
];