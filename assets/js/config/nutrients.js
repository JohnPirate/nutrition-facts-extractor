import {toLower} from 'lodash';

export const mustHaveNutrients = [
    'energy',
    'total_lipid_fat',
    'carbohydrate_by_difference',
    'fiber_total_dietary',
    'protein',
];

export const nutrientKeywords = {
    energy: [
        'Energie',
        'Energy',
        'Brennwert',
    ],
    total_lipid_fat: [
        'Fett',
        'Fat',
    ],
    fatty_acids_total_saturated: [
        'gesättigte Fettsäure',
        'davon gesättigt',
        'which saturates',
    ],
    fatty_acids_total_monounsaturated: [
        'Einfach ungesättigte Fettsäuren',
        'Einfach ungesättigte Fette',
        'einfach ungesättigt',
    ],
    fatty_acids_total_polyunsaturated: [
        'Mehrfach ungesättigte Fettsäuren',
        'Mehrfach ungesättigte Fette',
        'mehrfach ungesättigt',
    ],
    cholesterol: [
        'Cholesterin',
        'Cholesterol',
    ],
    carbohydrate_by_difference: [
        'Kohlenhydrate',
        'Carbohydrate',
    ],
    sugars_total: [
        'davon Zucker',
        'Zucker',
        'which sugars',
    ],
    starch: [
        'Stärke',
    ],
    sucrose: [
        'Saccharose',
        'Haushaltszucker'
    ],
    glucose_dextrose: [
        'Glucose',
        'Dextrose',
        'Traubenzucker',
    ],
    fructose: [
        'Fructose',
        'Fruktose',
        'Fruchtzucker',
    ],
    lactose: [
        'Lactose',
        'Milchzucker',
        'Laktose',
    ],
    maltose: [
        'Maltose',
        'Malzzucker',
    ],
    galactose: [
        'Galactose',
        'Galaktose',
        'Schleimzucker',
    ],
    fiber_total_dietary: [
        'Ballaststoffe',
        'Fiber',
        'Fibre',
    ],
    protein: [
        'Eiweiß',
        'Eiweiss',
        'Protein',
    ],
    salt: [
        'Salz',
        'Salt',
    ],
    caffeine: [
        'Koffein',
    ],
    alcohol_ethyl: [
        'Ethylalkohol',
        'Ethyl Alkohol',
    ],
    polyhydric_alcohols: [
        'mehrwertige Alkohole',
    ],
    biotin: [
        'Biotin',
    ],
    folate_total: [
        'Folat',
    ],
    retinol: [
        'Vitamin A1',
        'Retinol'
    ],
    vitamin_a_rae: [
        'Vitamin A',
    ],
    vitamin_b_12: [
        'Vitamin B12',
    ],
    thiamin: [
        'Vitamin B1',
        'Thiamin',
    ],
    riboflavin: [
        'Vitamin B2',
        'Riboflavin',
    ],
    niacin: [
        'Vitamin B3',
        'Niacin',
    ],
    pantothenic_acid: [
        'Vitamin B5',
        'Pantothenic acid',
    ],
    vitamin_b_6: [
        'Vitamin B6',
    ],
    vitamin_c_total_ascorbic_acid: [
        'Vitamin C',
        'ascorbic acid',
    ],
    vitamin_d2_ergocalciferol: [
        'Vitamin D2',
        'ergocalciferol',
    ],
    vitamin_d3_cholecalciferol: [
        'Vitamin D3',
        'cholecalciferol',
    ],
    vitamin_d_d2_d3: [
        'Vitamin D',
    ],
    vitamin_e_alpha_tocopherol: [
        'Vitamin E',
        'Alpha Tocopherol',
    ],
    vitamin_k_phylloquinone: [
        'Vitamin K',
    ],
    chromium_cr: [
        'Chrom',
    ],
    iron_fe: [
        'Eisen',
    ],
    iodinei: [
        'Jod',
    ],
    potassium_k: [
        'Kalium',
    ],
    calcium_ca: [
        'Kalzium',
        'Calcium',
    ],
    copper_cu: [
        'Kupfer',
    ],
    fluoride_f: [
        'Flourid',
    ],
    magnesium_mg: [
        'Magnesium',
    ],
    manganese_mn: [
        'Mangan',
    ],
    molybdenum_mo: [
        'Molybdän',
    ],
    sodium_na: [
        'Natrium',
    ],
    phosphorus_p: [
        'Phosphor',
    ],
    selenium_se: [
        'Selen',
    ],
    zinc_zn: [
        'Zink',
    ],
};

const map = {};
Object.keys(nutrientKeywords).forEach((nutrient) => {
    nutrientKeywords[nutrient].forEach((keyword) => {
        map[toLower(keyword)] = nutrient;
    });
});
export const nutrientKeywordsMap = map;