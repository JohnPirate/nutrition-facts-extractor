export const extractPortionsData = (txt) => {
    const portions = [];
    txt.split(/\r\n|\n/gi).forEach((line) => {
        let parsed = line.split('::');
        if (parsed.length === 5) {
            portions.push({
                value: parseFloat(parsed[0]),
                unit: parsed[1],
                amount: parseFloat(parsed[2]),
                label: parsed[3],
                lang: parsed[4]
            });
        }
    });
    return portions;
};