import {toLower} from 'lodash';
import {
    getValueFromObject,
    detectNutrientDetails,
    detectNutrientUnits,
    detectNumericValues,
} from '../utilities/helper';
import {nutrientKeywordsMap} from '../config/nutrients';

export const nutritionExtractData = (txt) => {

    //console.log('- Start parsing text');
    //console.log([txt]);

    const nutrients = {};

    let lastKeywordFound = null;
    let lastUnitFound = null;
    let findKeywordValue = false;

    txt.split("\n").forEach((line) => {
        line = line.trim();

        // Skip empty lines
        if (line === '') {
            return;
        }

        let data;

        // Search for keyword values
        let kword = getValueFromObject(
            nutrientKeywordsMap,
            toLower(line),
            Object.keys(nutrients)
        );
        if (kword) {
            lastKeywordFound = kword;
            findKeywordValue = true;

            if ((data = detectNutrientDetails(line))) {
                nutrients[lastKeywordFound] = data;
                findKeywordValue = false;
                return;
            }

            lastUnitFound = detectNutrientUnits(toLower(line));
        }


        if (!findKeywordValue) {
            return;
        }

        if (lastUnitFound) {
            let value = detectNumericValues(line);
            if (value.length === 0) {
                return;
            }
            nutrients[lastKeywordFound] = {
                unit: lastUnitFound,
                value: value[value.length - 1],
            };
            findKeywordValue = false;
            lastUnitFound = null;
        }

        if ((data = detectNutrientDetails(line))) {
            nutrients[lastKeywordFound] = data;
            findKeywordValue = false;
        }
    });

    return nutrients;
};