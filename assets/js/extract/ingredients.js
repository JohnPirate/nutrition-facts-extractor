import {
    words,
    capitalize,
} from 'voca';

/**
 * @param {String} txt
 * @returns {String[]}
 */
export const ingredientsExtractData = (txt) => {
    const ingredients = [];
    txt.split(/\,|:/gi).forEach((line) => {
        let ingrdient = [];
        words(line).forEach((word) => {
            if (isNaN(parseFloat(word))) {
                ingrdient.push(capitalize(word));
            }
        });
        if (ingrdient.length > 0) {
            ingredients.push(ingrdient.join(' '));
        }
    });

    return ingredients;
};