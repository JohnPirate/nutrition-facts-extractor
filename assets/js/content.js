import {nutritionSearchKeywords} from './config/search';
import {searchNutritionFacts} from './search/nutritionFacts';
import {ACTION_RESCAN_DOM} from './actions';

const startSearching = () => {
    nutritionSearchKeywords.some(searchNutritionFacts);
};

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action === ACTION_RESCAN_DOM) {
        startSearching();
        sendResponse(true);
        return;
    }
    sendResponse(false);
});

