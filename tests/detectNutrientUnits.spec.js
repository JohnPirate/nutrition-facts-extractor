import './setup';
import {detectNutrientUnits} from '../assets/js/utilities/helper';

describe('detectNutrientUnits', () => {

    it('returns null if no unit is found', () => {
        let simpleTxt = `This text has no nutrition details`;
        let n = detectNutrientUnits(simpleTxt);
        expect(n).toBe(null);
    });

    it('returns the unit if a unit is found', () => {
        expect(detectNutrientUnits('Protein (g)')).toStrictEqual('g');
        expect(detectNutrientUnits('Energie (kJ / kcal)')).toStrictEqual('kcal');
        expect(detectNutrientUnits('Fat mg')).toStrictEqual('mg');
        expect(detectNutrientUnits('Carbs: mg')).toStrictEqual('mg');
        expect(detectNutrientUnits('Salt 3.14 g')).toStrictEqual('g');
        expect(detectNutrientUnits('Fiber g 3.14')).toStrictEqual('g');
    });
});