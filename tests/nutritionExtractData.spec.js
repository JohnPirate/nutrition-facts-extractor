import './setup';
import {nutritionExtractData} from '../assets/js/extract/nutrition';
import {simpleNutritionTable1, simpleNutritionTable1Result} from './pages/nutrients/simpleNutritionTable1';
import {simpleNutritionTable2, simpleNutritionTable2Result} from './pages/nutrients/simpleNutritionTable2';
import {simpleNutritionTable3, simpleNutritionTable3Result} from './pages/nutrients/simpleNutritionTable3';
import {simpleNutritionTable4, simpleNutritionTable4Result} from './pages/nutrients/simpleNutritionTable4';
import {simpleNutritionTable5, simpleNutritionTable5Result} from './pages/nutrients/simpleNutritionTable5';
import {complexNutritionTable1, complexNutritionTable1Result} from './pages/nutrients/complexNutritionTable1';
import {multiNutritionTable1, multiNutritionTable1Result} from './pages/nutrients/multiNutritionTable1';

describe('nutritionExtractData', () => {

    it('returns an empty object if no nutrition fact is found', () => {
        let simpleTxt = `This
        Text has 
        no nutrition facts`;

        let n = nutritionExtractData(simpleTxt);
        expect(n).toStrictEqual({});
    });

    it('extracts a simple text version of a nutrition facts table', () => {
        let simpleTxt = `Energie
                         1405 kJ | 333 kcal

                         Fett
                         2,5 g

                         Kohlenhydrate
                         60 g

                         Eiweiß
                         12 g
                        `;

        let n = nutritionExtractData(simpleTxt);
        expect(n).toStrictEqual({
            energy: {
                value: 333,
                unit: 'kcal',
            },
            total_lipid_fat: {
                value: 2.5,
                unit: 'g',
            },
            carbohydrate_by_difference: {
                value: 60,
                unit: 'g',
            },
            protein: {
                value: 12,
                unit: 'g',
            },
        });
    });

    it('extract nutrition facts with artifacts of nutrient infos', () => {
        let simpleTxt = `Energie
                         

                         Fett
                         2,5 g

                         Kohlenhydrate
                         

                         Eiweiß
                         12 g
                        `;

        let n = nutritionExtractData(simpleTxt);
        expect(n).toStrictEqual({
            total_lipid_fat: {
                value: 2.5,
                unit: 'g',
            },
            protein: {
                value: 12,
                unit: 'g',
            },
        });
    });

    it('extract nutrition facts from simple nutrition facts table 1', () => {
        let n = nutritionExtractData(simpleNutritionTable1);
        expect(n).toStrictEqual(simpleNutritionTable1Result);
    });

    it('extract nutrition facts from simple nutrition facts table 2', () => {
        let n = nutritionExtractData(simpleNutritionTable2);
        expect(n).toStrictEqual(simpleNutritionTable2Result);
    });

    it('extract nutrition facts from simple nutrition facts table 3', () => {
        let n = nutritionExtractData(simpleNutritionTable3);
        expect(n).toStrictEqual(simpleNutritionTable3Result);
    });

    it('extract nutrition facts from simple nutrition facts table 4', () => {
        let n = nutritionExtractData(simpleNutritionTable4);
        expect(n).toStrictEqual(simpleNutritionTable4Result);
    });

    it('extract nutrition facts from simple nutrition facts table 5', () => {
        let n = nutritionExtractData(simpleNutritionTable5);
        expect(n).toStrictEqual(simpleNutritionTable5Result);
    });

    it('extract nutrition facts from a complex nutrition facts table 1', () => {
        let n = nutritionExtractData(complexNutritionTable1);
        expect(n).toStrictEqual(complexNutritionTable1Result);
    });

    it('extract nutrition facts from a multi nutrition facts table 1', () => {
        multiNutritionTable1.forEach((tbl, idx) => {
            let n = nutritionExtractData(tbl);
            expect(n).toStrictEqual(multiNutritionTable1Result[idx]);
        });
    });
});