import './setup';
import {detectNumericValues} from '../assets/js/utilities/helper';

describe('detectNutrientUnits', () => {

    it('returns an empty array if no numeric values are found', () => {
        let simpleTxt = `This text has no numeric values`;
        let n = detectNumericValues(simpleTxt);
        expect(n).toStrictEqual([]);
        expect(detectNumericValues('test1')).toStrictEqual([]);
        expect(detectNumericValues('test2.3')).toStrictEqual([]);
        expect(detectNumericValues('test4,5')).toStrictEqual([]);
    });

    it('returns an array of numeric values if numeric values are found', () => {
        expect(detectNumericValues('0')).toStrictEqual([0]);
        expect(detectNumericValues('1 2 3.14')).toStrictEqual([1,2, 3.14]);
        expect(detectNumericValues('3.14 / 2,72')).toStrictEqual([3.14, 2.72]);
        expect(detectNumericValues('Energie (kJ | kcal) (3.14 | 2,72)')).toStrictEqual([3.14, 2.72]);
    });
});