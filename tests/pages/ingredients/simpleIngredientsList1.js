export const simpleIngredientsList1 = `54,9% Apfelsaft, 21% Bananenpüree,11% Spinatpüree, 8% Birnenpüree, 4% Grünkohlpüree, 1% Ingwerpüree, 0,1% Matcha`;

export const simpleIngredientsList1Result = [
    'Apfelsaft',
    'Bananenpüree',
    'Spinatpüree',
    'Birnenpüree',
    'Grünkohlpüree',
    'Ingwerpüree',
    'Matcha',
];
