import '../../setup';
import {detectIngredients} from '../../../assets/js/detect/ingredients';
import {simpleIngredientsList1} from './simpleIngredientsList1';

describe('findIngredientListsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/ingredients/simpleIngredientsList1.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a simple ingredients list 1', () => {
        global.$ = require('jquery');
        expect(detectIngredients('Zutaten')).toBe(simpleIngredientsList1);
    });
});