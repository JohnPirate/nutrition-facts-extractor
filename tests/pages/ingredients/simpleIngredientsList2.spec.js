import '../../setup';
import {detectIngredients} from '../../../assets/js/detect/ingredients';
import {simpleIngredientsList2} from './simpleIngredientsList2';

describe('findIngredientListsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/ingredients/simpleIngredientsList2.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a simple ingredients list 2', () => {
        global.$ = require('jquery');
        expect(detectIngredients('Zutaten')).toBe(simpleIngredientsList2);
    });
});