export const simpleIngredientsList2 = `64% Kartoffeln*, Sonnenblumenöl*, Meersalz, Gewürzextrakte, 0,8% Paprikapulver*, natürliches Aroma, Tomatenpulver*, Zwiebelpulver*, Zucker*, Knoblauch*, Piment*, Säuerungsmittel: Citronensäure`;

export const simpleIngredientsList2Result = [
    'Kartoffeln',
    'Sonnenblumenöl',
    'Meersalz',
    'Gewürzextrakte',
    'Paprikapulver',
    'Natürliches Aroma',
    'Tomatenpulver',
    'Zwiebelpulver',
    'Zucker',
    'Knoblauch',
    'Piment',
    'Säuerungsmittel',
    'Citronensäure',
];
