export const simpleNutritionTable5 = `Nährwertdeklaration per
100ml
250ml
%*
Brennwert
100ml180 kJ / 42 kcal
250ml450 kJ / 105 kcal
%*5%
Fett
100ml0 g
250ml0 g
%*0%
davon gesättigte Fettsäuren
100ml0 g
250ml0 g
%*0%
Kohlenhydrate
100ml10,6 g
250ml27 g
%*10%
davon Zucker
100ml10,6 g
250ml27 g
%*29%
Eiweiß
100ml0 g
250ml0 g
%*0%
Salz
100ml0 g
250ml0 g
%*0%
`;

export const simpleNutritionTable5Result = {
    energy: {
        value: 42,
        unit: 'kcal',
    },
    total_lipid_fat: {
        value: 0,
        unit: 'g',
    },
    fatty_acids_total_saturated: {
        value: 0,
        unit: 'g',
    },
    carbohydrate_by_difference: {
        value: 10.6,
        unit: 'g',
    },
    sugars_total: {
        value: 10.6,
        unit: 'g',
    },
    protein: {
        value: 0,
        unit: 'g',
    },
    salt: {
        value: 0,
        unit: 'g',
    },
};