import '../../setup';
import {detectNutritionFacts} from '../../../assets/js/detect/nutritionFacts';
import {simpleNutritionTable4} from './simpleNutritionTable4';

describe('findNutritionFactsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/nutrients/simpleNutritionTable4.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a simple nutrition facts table 4', () => {
        global.$ = require('jquery');
        let txt = detectNutritionFacts('Brennwert');
        expect(txt).toBe(simpleNutritionTable4);
    });
});