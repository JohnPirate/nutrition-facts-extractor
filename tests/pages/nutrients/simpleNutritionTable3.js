export const simpleNutritionTable3 = `im Durchschnitt
je 100 g
je Riegel
(21 g)
%* je Riegel
(21 g)
Energie (kJ / kcal)
2360 / 566
496 / 119
6
Fett (g)
35,0
7,4
11
— davon gesättigte Fettsäuren (g)
22,6
4,7
24
Kohlenhydrate (g)
53,5
11,2
4
— davon Zucker (g)
53,3
11,2
12
Eiweiss (g)
8,7
1,8
4
Salz (g)
0,313
0,066
1
* Referenzmenge für einen durchschnittlichen Erwachsenen (8 400 kJ / 2 000 kcal).
`;

export const simpleNutritionTable3Result = {
    energy: {
        value: 566,
        unit: 'kcal',
    },
    total_lipid_fat: {
        value: 35,
        unit: 'g',
    },
    fatty_acids_total_saturated: {
        value: 22.6,
        unit: 'g',
    },
    carbohydrate_by_difference: {
        value: 53.5,
        unit: 'g',
    },
    sugars_total: {
        value: 53.3,
        unit: 'g',
    },
    protein: {
        value: 8.7,
        unit: 'g',
    },
    salt: {
        value: 0.313,
        unit: 'g',
    },
};