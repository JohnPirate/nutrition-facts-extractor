export const simpleNutritionTable1 = `Durchschnittliche Nährwertangaben
100g
30g
%* (30 g)
Energie
1601 kJ
378 kcal
480 kJ
113 kcal
6 %
Fett
3.1 g
0.9 g
1 %
- Fett, davon gesättigte Fettsäuren
0.3 g
< 0.1 g
< 1 %
Kohlenhydrate
73 g
22 g
8 %
- Kohlenhydrate, davon Zucker
0.7 g
< 0.5 g
< 1 %
Ballaststoffe
4.3 g
1.3 g
Eiweiß
12 g
3.6 g
7 %
Salz
4.5 g
1.4 g
23 %
`;

export const simpleNutritionTable1Result = {
    energy: {
        value: 378,
        unit: 'kcal',
    },
    total_lipid_fat: {
        value: 3.1,
        unit: 'g',
    },
    fatty_acids_total_saturated: {
        value: 0.3,
        unit: 'g',
    },
    carbohydrate_by_difference: {
        value: 73,
        unit: 'g',
    },
    sugars_total: {
        value: 0.7,
        unit: 'g',
    },
    fiber_total_dietary: {
        value: 4.3,
        unit: 'g',
    },
    protein: {
        value: 12,
        unit: 'g',
    },
    salt: {
        value: 4.5,
        unit: 'g',
    },
};
