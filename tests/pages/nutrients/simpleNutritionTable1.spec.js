import '../../setup';
import {detectNutritionFacts} from '../../../assets/js/detect/nutritionFacts';
import {simpleNutritionTable1} from './simpleNutritionTable1';

describe('findNutritionFactsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/nutrients/simpleNutritionTable1.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a simple nutrition facts table 1', () => {
        global.$ = require('jquery');
        let txt = detectNutritionFacts('Energie');
        expect(txt).toBe(simpleNutritionTable1);
    });
});