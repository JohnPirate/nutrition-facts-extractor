export const simpleNutritionTable4 = `Brennwert kJ (kcal)225 (53)
Fett0g
davon gesättigte Fettsäuren0g
Kohlenhydrate11,6g
davon Zucker10,5g
Ballaststoffe1,1g
Eiweiß0,8g
Salz0g
`;

export const simpleNutritionTable4Result = {
    energy: {
        value: 53,
        unit: 'kcal',
    },
    total_lipid_fat: {
        value: 0,
        unit: 'g',
    },
    fatty_acids_total_saturated: {
        value: 0,
        unit: 'g',
    },
    carbohydrate_by_difference: {
        value: 11.6,
        unit: 'g',
    },
    sugars_total: {
        value: 10.5,
        unit: 'g',
    },
    fiber_total_dietary: {
        value: 1.1,
        unit: 'g',
    },
    protein: {
        value: 0.8,
        unit: 'g',
    },
    salt: {
        value: 0,
        unit: 'g',
    },
};