import '../../setup';
import {detectNutritionFacts} from '../../../assets/js/detect/nutritionFacts';
import {simpleNutritionTable2} from './simpleNutritionTable2';

describe('findNutritionFactsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/nutrients/simpleNutritionTable2.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a simple nutrition facts table 2', () => {
        global.$ = require('jquery');
        let txt = detectNutritionFacts('Energie');
        expect(txt).toBe(simpleNutritionTable2);
    });
});