export const simpleNutritionTable2 = `DurchschnittlicheNährwerte
Produkt enthältdurchschnittlich
Unzubereitet
100 g
Energie
2225 kJ | 534 kcal
Fett
33 g
Fett, davon gesättigte Fettsäuren
2,7 g
Kohlenhydrate
52 g
Kohlenhydrate, davon Zucker
0,5 g
Ballaststoffe
4,5 g
Eiweiß
4,5 g
Salz
1,7 g
Packung (200 G) enthält ca. 8 Portion(en)
Broteinheiten
4,3 BE
`;

export const simpleNutritionTable2Result = {
    energy: {
        value: 534,
        unit: 'kcal',
    },
    total_lipid_fat: {
        value: 33,
        unit: 'g',
    },
    fatty_acids_total_saturated: {
        value: 2.7,
        unit: 'g',
    },
    carbohydrate_by_difference: {
        value: 52,
        unit: 'g',
    },
    sugars_total: {
        value: 0.5,
        unit: 'g',
    },
    fiber_total_dietary: {
        value: 4.5,
        unit: 'g',
    },
    protein: {
        value: 4.5,
        unit: 'g',
    },
    salt: {
        value: 1.7,
        unit: 'g',
    },
};
