import '../../setup';
import {detectNutritionFacts} from '../../../assets/js/detect/nutritionFacts';
import {simpleNutritionTable3} from './simpleNutritionTable3';

describe('findNutritionFactsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/nutrients/simpleNutritionTable3.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a simple nutrition facts table 3', () => {
        global.$ = require('jquery');
        let txt = detectNutritionFacts('Energie');
        expect(txt).toBe(simpleNutritionTable3);
    });
});