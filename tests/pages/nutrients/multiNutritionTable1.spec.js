import '../../setup';
import {detectNutritionFacts} from '../../../assets/js/detect/nutritionFacts';
import {multiNutritionTable1} from './multiNutritionTable1';

describe('findNutritionFactsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/nutrients/multiNutritionTable1.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds some multi nutrition facts tables 1', () => {
        global.$ = require('jquery');
        let txt = detectNutritionFacts('Brennwert');
        expect(txt).toStrictEqual(multiNutritionTable1);
    });
});