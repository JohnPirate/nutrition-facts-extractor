export const complexNutritionTable1 = `Produktfakten
Nährwertangaben
per 100g
pro Scheibe
Brennwert
1687 kJ (401 kcal)
236 kJ (56 kcal)
Eiweiß
13,5 g
1,9 g
Kohlenhydrate
61,5 g
8,6 g
davon Zucker
2,0 g
0,3 g
Fett
9,5 g
1,3 g
davon gesättigte Fettsäuren
2,5 g
0,4 g
Ballaststoffe
7,5 g
1,1 g
Salz
1,25 g
0,175 g
`;

export const complexNutritionTable1Result = {
    energy: {
        value: 401,
        unit: 'kcal',
    },
    total_lipid_fat: {
        value: 9.5,
        unit: 'g',
    },
    fatty_acids_total_saturated: {
        value: 2.5,
        unit: 'g',
    },
    carbohydrate_by_difference: {
        value: 61.5,
        unit: 'g',
    },
    sugars_total: {
        value: 2,
        unit: 'g',
    },
    fiber_total_dietary: {
        value: 7.5,
        unit: 'g',
    },
    protein: {
        value: 13.5,
        unit: 'g',
    },
    salt: {
        value: 1.25,
        unit: 'g',
    },
};
