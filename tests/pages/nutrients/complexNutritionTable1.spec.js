import '../../setup';
import {detectNutritionFacts} from '../../../assets/js/detect/nutritionFacts';
import {complexNutritionTable1} from './complexNutritionTable1';

describe('findNutritionFactsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/nutrients/complexNutritionTable1.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a complex nutrition facts table 1', () => {
        global.$ = require('jquery');
        let txt = detectNutritionFacts('Brennwert');
        expect(txt).toBe(complexNutritionTable1);
    });
});