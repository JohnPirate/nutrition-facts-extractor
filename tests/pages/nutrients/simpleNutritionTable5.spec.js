import '../../setup';
import {detectNutritionFacts} from '../../../assets/js/detect/nutritionFacts';
import {simpleNutritionTable5} from './simpleNutritionTable5';

describe('findNutritionFactsInDom', () => {

    before(function() {
        return JSDOM.fromFile('tests/pages/nutrients/simpleNutritionTable5.html')
            .then((dom) => {
                global.window = dom.window;
                global.document = window.document;
            });
    });

    it('finds a simple nutrition facts table 5', () => {
        global.$ = require('jquery');
        let txt = detectNutritionFacts('Brennwert');
        expect(txt).toBe(simpleNutritionTable5);
    });
});