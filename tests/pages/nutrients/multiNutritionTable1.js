export const multiNutritionTable1 = [
`Nährwertangaben
Pro
Portion
(110g)
(%)
Brennwert
747kJ / 177
kcal
(9%)
Fett
2.8g
(4%)
davon
gesättigte
Fettsäuren
0.2g
(1%)
Kohlenhydrate
33g
(13%)
davon
Zucker
<0.5g
(<1%)
Ballaststoffe
1.4g
Eiweiß
4.3g
(9%)
Salz
0.02g
(<1%)
`,
`Nährwertangaben
Pro
100g
Brennwert
679kJ/
161kcal
Fett
2.5g
davon
gesättigte
Fettsäuren
0.2g
Kohlenhydrate
30g
davon
Zucker
<0.5g
Ballaststoffe
1.3g
Eiweiß
3.9g
Salz
0.02g
`
];

export const multiNutritionTable1Result = [
    {
        energy: {
            value: 177,
            unit: 'kcal',
        },
        total_lipid_fat: {
            value: 2.8,
            unit: 'g',
        },
        fatty_acids_total_saturated: {
            value: 0.2,
            unit: 'g',
        },
        carbohydrate_by_difference: {
            value: 33,
            unit: 'g',
        },
        sugars_total: {
            value: 0.5,
            unit: 'g',
        },
        fiber_total_dietary: {
            value: 1.4,
            unit: 'g',
        },
        protein: {
            value: 4.3,
            unit: 'g',
        },
        salt: {
            value: 0.02,
            unit: 'g',
        },
    },
    {
        energy: {
            value: 161,
            unit: 'kcal',
        },
        total_lipid_fat: {
            value: 2.5,
            unit: 'g',
        },
        fatty_acids_total_saturated: {
            value: 0.2,
            unit: 'g',
        },
        carbohydrate_by_difference: {
            value: 30,
            unit: 'g',
        },
        sugars_total: {
            value: 0.5,
            unit: 'g',
        },
        fiber_total_dietary: {
            value: 1.3,
            unit: 'g',
        },
        protein: {
            value: 3.9,
            unit: 'g',
        },
        salt: {
            value: 0.02,
            unit: 'g',
        },
    },
];
