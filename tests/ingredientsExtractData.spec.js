import './setup';
import {ingredientsExtractData} from '../assets/js/extract/ingredients';
import {simpleIngredientsList1, simpleIngredientsList1Result} from './pages/ingredients/simpleIngredientsList1';
import {simpleIngredientsList2, simpleIngredientsList2Result} from './pages/ingredients/simpleIngredientsList2';

describe('ingredientsExtractData', () => {

    it('extract ingredients from simple ingredients list 1', () => {
        expect(ingredientsExtractData(simpleIngredientsList1)).toStrictEqual(simpleIngredientsList1Result);
    });

    it('extract ingredients from simple ingredients list 2', () => {
        expect(ingredientsExtractData(simpleIngredientsList2)).toStrictEqual(simpleIngredientsList2Result);
    });

});