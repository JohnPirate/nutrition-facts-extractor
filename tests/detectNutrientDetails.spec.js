import './setup';
import {detectNutrientDetails} from '../assets/js/utilities/helper';

describe('detectNutrientDetails', () => {

    it('returns null if no details are found', () => {
        let simpleTxt = `This text has no nutrition details`;
        let n = detectNutrientDetails(simpleTxt);
        expect(n).toBe(null);
        expect(detectNutrientDetails('13,2')).toBe(null);
        expect(detectNutrientDetails('13.2')).toBe(null);
    });

    it('returns the value and the unit if details are found', () => {
        let detail = {
            value: 0,
            unit: 'g',
        };
        expect(detectNutrientDetails('0g')).toStrictEqual(detail);
        expect(detectNutrientDetails('0 g')).toStrictEqual(detail);

        detail = {
            value: 12,
            unit: 'mg',
        };
        expect(detectNutrientDetails('12mg')).toStrictEqual(detail);
        expect(detectNutrientDetails('12 mg')).toStrictEqual(detail);

        detail = {
            value: 12.12,
            unit: 'µg',
        };
        expect(detectNutrientDetails('12,12µg')).toStrictEqual(detail);
        expect(detectNutrientDetails('12.12 µg')).toStrictEqual(detail);

        detail = {
            value: 360,
            unit: 'g',
        };
        expect(detectNutrientDetails('360g')).toStrictEqual(detail);
        expect(detectNutrientDetails('360,00 g')).toStrictEqual(detail);

        detail = {
            value: 12,
            unit: 'g',
        };
        expect(detectNutrientDetails('100ml12g')).toStrictEqual(detail);
        expect(detectNutrientDetails('100ml 12g')).toStrictEqual(detail);

        detail = {
            value: 3.1415,
            unit: 'kcal',
        };
        expect(detectNutrientDetails('Energie: 3,1415 kcal')).toStrictEqual(detail);
        expect(detectNutrientDetails('Energie - 3.1415kcal')).toStrictEqual(detail);
        expect(detectNutrientDetails('Energie | 3.1415 | kcal')).toStrictEqual(detail);
        expect(detectNutrientDetails('Energie3.1415kcal')).toStrictEqual(detail);
    });
});