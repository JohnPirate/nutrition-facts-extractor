#!/usr/bin/env bash

node_modules/.bin/mocha tests/pages/ingredients/simpleIngredientsList1.spec.js \
&& node_modules/.bin/mocha tests/pages/ingredients/simpleIngredientsList2.spec.js \
&& node_modules/.bin/mocha tests/pages/nutrients/simpleNutritionTable1.spec.js \
&& node_modules/.bin/mocha tests/pages/nutrients/simpleNutritionTable2.spec.js \
&& node_modules/.bin/mocha tests/pages/nutrients/simpleNutritionTable3.spec.js \
&& node_modules/.bin/mocha tests/pages/nutrients/simpleNutritionTable4.spec.js \
&& node_modules/.bin/mocha tests/pages/nutrients/simpleNutritionTable5.spec.js \
&& node_modules/.bin/mocha tests/pages/nutrients/complexNutritionTable1.spec.js \
&& node_modules/.bin/mocha tests/pages/nutrients/multiNutritionTable1.spec.js \
&& node_modules/.bin/mocha tests
